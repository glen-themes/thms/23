![Screenshot preview of the theme "Lover's Leap" by glenthemes](https://68.media.tumblr.com/9daafa40faf092911a40b7943bf543ab/tumblr_opmil2MDjX1qg2f5co1_540.gif)

**Theme no.:** 23  
**Theme name:** Lover's Leap  
**Theme type:** Free / Tumblr use  
**Description:** A RP blog-inspired theme featuring Xayah and Rakan from League of Legends.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2017-04-12](https://64.media.tumblr.com/9b48cd37b10b4c4cae2bf829dd568022/tumblr_nrmwzjerYU1ubolzro3_r1_540.gif)

**Post:** [glenthemes.tumblr.com/post/159488746924](https://glenthemes.tumblr.com/post/159488746924)  
**Preview [1]:** [glenthpvs.tumblr.com/rakan](https://glenthpvs.tumblr.com/rakan)  
**Preview [2]:** [glenthpvs.tumblr.com/xayah](https://glenthpvs.tumblr.com/xayah)  
**Download:** [pastebin.com/UVMBVary](https://pastebin.com/UVMBVary)
